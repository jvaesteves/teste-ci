FROM gradle:5.2.1-jdk8 AS build
COPY --chown=gradle:gradle . /home/gradle
WORKDIR /home/gradle
RUN gradle build

FROM plantuml/plantuml-server:tomcat
WORKDIR /
COPY --from=build /home/gradle/build/libs/plantuml-encorder-1.0-SNAPSHOT.jar plantuml-encorder.jar
COPY entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh && apt update && apt install -y wget
ENTRYPOINT [ "/entrypoint.sh" ]