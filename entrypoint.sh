#!/bin/bash
set -e
/usr/local/tomcat/bin/catalina.sh run >> /dev/null 2>&1 &
sleep 50
exec "$@"