import net.sourceforge.plantuml.code.Transcoder;
import net.sourceforge.plantuml.code.TranscoderUtil;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Main {

    public static void main(String[] args) {
        try {
            Transcoder transcoder = TranscoderUtil.getDefaultTranscoder();
            String payload = new String(Files.readAllBytes(Paths.get(args[0])), StandardCharsets.UTF_8);
            String encodedPayload = transcoder.encode(payload);
            System.out.print(encodedPayload);
        }
        catch (IOException ex) { }
    }

}
